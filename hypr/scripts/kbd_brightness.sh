#!/bin/bash

ARGUMENT=$1
MAX='3'
MIN='0'

add_brightness () {
    if [ -z $(asusctl -k | grep $MAX) ]; then
        asusctl -n
    else
        exit 0
    fi
}

reduce_brightness () {
    if [ -z $(asusctl -k | grep $MIN) ]; then
        asusctl -p
    else
        exit 0
    fi
}

change_brightness () {
    if [ $ARGUMENT == "--up" ]; then
        add_brightness

    else
        reduce_brightness
    fi
}


if [ $ARGUMENT == "--up" ] || [ $ARGUMENT == "--down" ] ; then
    #change_brightness
    change_brightness
    exit 0

else
    echo "No/Incorrect argument supplied"
    exit 0
fi

